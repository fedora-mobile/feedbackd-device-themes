Name:           feedbackd-device-themes
Version:        1602d415aed30b1a67c0ff270551230725b8ef92
Release:        rc4%{?dist}
Summary:        Feedbackd themes for different devices

License:        GPLv3+
URL:            https://source.puri.sm/Librem5/feedbackd-device-themes
Source0:        https://source.puri.sm/Librem5/feedbackd-device-themes/-/archive/%{version}/feedbackd-device-themes-%{version}.tar.gz


%description
Feedbackd themes for different devices such as PinePhone and Librem5

%global debug_package %{nil}

%prep
%autosetup -p1 -c

%install
mv data/pine64,pinephone.json %{_datadir}/feedbackd/themes/pinephone.json
mv data/purism,librem5.json %{_datadir}/feedbackd/themes/librem5.json


%files
%license COPYING
%{_datadir}/feedbackd/themes/pinephone.json
%{_datadir}/feedbackd/themes/librem5.json

%changelog
* Sat Jan 23 2021 Tor <torbuntu@fedoraproject.org> - 1602d415aed30b1a67c0ff270551230725b8ef92
- Initial packaging
